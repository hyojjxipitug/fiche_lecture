@tool
extends Label

@onready var gameTimer := $gameTimer

@export var TotalTime :int:
	set(new_value):
		TotalTime = new_value
		_set_text(TotalTime)

signal timer_finished

func _ready():
	gameTimer.timeout.connect(func(): timer_finished.emit())

func _set_text(time: int) -> void:
	text = str(time) + " seconde"
	if time > 1:
		text += 's'

func _process(delta) -> void:
	if not Engine.is_editor_hint():
		var tl: int = int(gameTimer.time_left)
		_set_text(tl)

func start_timer():
	if not gameTimer.is_stopped():
		gameTimer.stop()
	gameTimer.start(TotalTime)
