@tool
extends Label

@export var score: int = 0:
	set(new_score):
		score = new_score
		self.text = 'Score: ' + str(score) + ' mot'
		if score > 1:
			self.text += 's'
