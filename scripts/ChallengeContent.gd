@tool
extends RichTextLabel


@export_group('Grid layout')
@export var maxColumns: int = 7
@export var currentIndex: int = 0:
	set(new_value):
		currentIndex = new_value
		_render_content()
@export_group('Default Words')
# the variables below are only used in the editor to quickly fine tune the challenge rendering. Could have been a good old lorem ipsum
@export var defaultSyllabes := ["ul", "lir", "sal", "le", "bou", "var", "to", "pu", "bul", "my", "ten", "ra", "sa", "mol", "bil", "fim", "or", "my", "ris", "fé", "tir", "nu", "be", "ar", "is", "mu", "ne", "nil", "ni", "tir", "al", "rè", "bac", "ni", "ral", "chê", "vur", "ma", "né", "pul", "si", "bê", "nè", "ni", "tal", "ma", "ba", "fol", "sol"]
@export var defaultWords := ["vite", "sirop", "pari", "toujours", "mare", "lilas", "gant", "bizarre", "luc", "lis", "vélo", "papa", "bébé", "mie", "mur", "mamie", "lira", "pari", "annulé", "papi", "mil", "remue", "pape", "tomate", "série", "léna", "mil", "mamie"]
@export var defaultUtilWords := ["de", "avec", "de", "un", "car", "un", "de"]

var syllabes := []
var words := []
var utilWords := []

const CENTER_START = "[center]"
const CENTER_STOP = "[/center]"
const CELL_START = "[cell]"
const CELL_STOP = "[/cell]"
const TABLE_START = "[table=%d]"
const TABLE_STOP = "[/table]"
const WORD_PAD = "  "
const COLOR_HIGHLIGHT_START = "[color=#156317]"
const COLOR_GRAY_START = "[color=dark_gray]"
const COLOR_STOP = "[/color]"

func _ready():
	syllabes = defaultSyllabes
	words = defaultWords
	utilWords = defaultUtilWords
	_render_content()

func _render_content():
	var index := 0
	
	text = "Syllabes:\n"
	text += CENTER_START
	text += TABLE_START % maxColumns
	for cell in syllabes:
		text += CELL_START
		if index < currentIndex:
			text += COLOR_GRAY_START
		elif index == currentIndex:
			text += COLOR_HIGHLIGHT_START
		text += WORD_PAD + cell + WORD_PAD
		if index <= currentIndex:
			text += COLOR_STOP
		text += CELL_STOP
		index += 1
	text += TABLE_STOP
	text += CENTER_STOP
	text += "\n\n\n"
	
	text += "Mots:\n"
	text += CENTER_START
	text += TABLE_START % maxColumns
	for cell in words:
		text += CELL_START
		if index < currentIndex:
			text += COLOR_GRAY_START
		elif index == currentIndex:
			text += COLOR_HIGHLIGHT_START
		text += WORD_PAD + cell + WORD_PAD
		if index <= currentIndex:
			text += COLOR_STOP
		text += CELL_STOP
		index += 1
	text += TABLE_STOP
	text += CENTER_STOP
	text += "\n\n\n"
	
	text += "Mots outils:\n"
	text += CENTER_START
	text += TABLE_START % maxColumns
	for cell in utilWords:
		text += CELL_START
		if index < currentIndex:
			text += COLOR_GRAY_START
		elif index == currentIndex:
			text += COLOR_HIGHLIGHT_START
		text += WORD_PAD + cell + WORD_PAD
		if index <= currentIndex:
			text += COLOR_STOP
		text += CELL_STOP
		index += 1
	text += TABLE_STOP
	text += CENTER_STOP
	text += "\n\n\n"
	

func set_content(syllabes, words, utilWords):
	self.syllabes = syllabes
	self.words = words
	self.utilWords = utilWords
	_render_content()
