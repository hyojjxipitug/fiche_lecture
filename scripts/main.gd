extends Control


@onready var ScoreHolder := $ChallengeContainer/ScoreHolder
@onready var TimerManager := $ChallengeContainer/TimerManager
@onready var ChallengeContent := $ChallengeContainer/ChallengeContent
@onready var actionBtn := $GeneralContainer/ActionButton
@onready var InfoLabel := $GeneralContainer/InfoLabel
@onready var ChallengeContainer := $ChallengeContainer
@onready var GeneralContainer := $GeneralContainer

@export_group('Challenge parameters')
@export var syllabesCount: int = 49
@export var wordsCount: int = 28
@export var utilWordsCount: int = 7
@export_group('Misc')
@export var score: int = 0: 
	set(new_score):
		score = new_score
		ScoreHolder.score = score
		ChallengeContent.currentIndex = score


const WORDS_PATH := 'res://assets/words.json'
const SUCCESS_MSG := 'Félicitations ! Tu as lu l\'entièreté de la fiche en %d secondes'
const TIME_OUT_MSG := 'Bravo! Tu as lu %d mots en %d secondes'
const REPLAY_MSG := '\n\n\nAppuyez sur le bouton GO! pour rejouer'
const START_MSG := 'Appuyez sur le bouton GO! pour commencer'

var allSyllabes := []
var allWords := []
var allUtil_words := []

var challengeSyllabes := []
var challengeWords := []
var challengeUtil_words := []

func _ready() -> void:
	# load words from assets
	_load_assets()

func _input(event) -> void:
	# Is space key pressed?
	if event is InputEventKey and event.pressed and event.keycode == KEY_SPACE:
		# are we in the challenge view?
		if ChallengeContainer.is_visible_in_tree():
			score += 1
			if score >= len(challengeSyllabes) + len(challengeWords) + len(challengeUtil_words):
				var time_left = TimerManager.gameTimer.time_left
				InfoLabel.text = SUCCESS_MSG % (TimerManager.TotalTime - int(time_left))
				InfoLabel.text += REPLAY_MSG
				_reset()
		# are we in the main menu view?
		else:
			_on_button_pressed()
	# escape key to main menu
	if event is InputEventKey and event.pressed and event.keycode == KEY_ESCAPE and ChallengeContainer.is_visible_in_tree():
		InfoLabel.text = START_MSG
		_reset()

func _load_assets() -> void:
	# read word list from static file in project
	var json_as_text = FileAccess.get_file_as_string(WORDS_PATH)
	var json_as_dict = JSON.parse_string(json_as_text)
	
	# filter tokens by category
	allSyllabes = json_as_dict['words'].filter(func(x): return x['type'] == 'syllabe').map(func(x): return x['value'])
	allWords = json_as_dict['words'].filter(func(x): return x['type'] == 'mot').map(func(x): return x['value'])
	allUtil_words = json_as_dict['words'].filter(func(x): return x['type'] == 'mot outil').map(func(x): return x['value'])

func _reset():
	# reset game state for new exercise
	TimerManager.gameTimer.stop()
	ChallengeContainer.hide()
	GeneralContainer.show()
	score = 0

func _prepare_challenge():
	# pick random words for new challenge
	challengeSyllabes = range(syllabesCount).map(func(x): return allSyllabes.pick_random())
	challengeWords = range(wordsCount).map(func(x): return allWords.pick_random())
	challengeUtil_words = range(utilWordsCount).map(func(x): return allUtil_words.pick_random())

func _on_button_pressed():
	# prepare and display new challenge
	_prepare_challenge()
	ChallengeContent.set_content(challengeSyllabes, challengeWords, challengeUtil_words)
	GeneralContainer.hide()
	ChallengeContainer.show()
	TimerManager.start_timer()

func _on_timer_manager_timer_finished():
	# end challenge upon timer end signal (wiring done via editor)
	var time_left = TimerManager.gameTimer.time_left
	InfoLabel.text = TIME_OUT_MSG % [score, TimerManager.TotalTime]
	InfoLabel.text += REPLAY_MSG
	_reset()
