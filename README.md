# fiche_lecture

Ce projet est une application de drill de lecture pour des élèves de 1e primaire (6 ans). Il est construit avec le framework Godot 4.
Chaque nouveau commit sur la branche *main* déclenche l'exécution du pipeline correspondant, le résultat étant déployé dans l'espace *Gitlab Pages* du projet (TODO add url)

## Utilisation

L'application web est résumée à sa plus simple expression: cliquez sur le bouton GO! en bas de l'interface de départ et un exercice de lecture aléatoire est généré. Faite lire l'enfant et appuyez sur la barre espace à chaque mot lu dans l'ordre. Au bout d'une minute, un message récapitulatif est affiché donnant le score final. Un nouvel exercice peut alors être à nouveau généré de la même manière.

## License

Ce projet est libre de réutilisation sans aucune contrainte. Aucune garantie n'est fournie quant à son utilité. De l'aide peut-être fournie *on a best effort basis* (c'est à dire: je fais de mon mieux mais aucune promesse)

